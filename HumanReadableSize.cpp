// Copyright 2022
// Author: Harry <daniel.h.peter@gmail.com>

#include "./HumanReadableSize.h"

HRSize toHR(uint64_t bytes) {
  HRSize hr;
  uint64_t size = bytes;
  const char *units[5] = { "B", "KB", "MB", "GB", "TB"};
  hr._unit = units[0];
  hr._number = size;
  for (int i = 1; size >= 1000; i++) {
    hr._unit = units[i];
    hr._number = size / 1000;
    size /= 1000;
  }
  return hr;
}
