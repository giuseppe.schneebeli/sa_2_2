// Copyright 2022
// Author: Harry <daniel.h.peter@gmail.com>

#ifndef FILE_H_
#define FILE_H_

#include <string>
#include <chrono> // NOLINT [build/c++11]
#include "./Directory.h"
#include "./HumanReadableSize.h"

class File {
 public:
  File();

  // function to calc size of a File
  uint64_t calcSize(const std::string path);

  // Function to get the filename from a path
  // https://stackoverflow.com/questions/8520560/get-a-file-name-from-a-path
  std::string getFileName(const std::string path);

  // Function to get the parent folder path
  std::string getLocation(const std::string path);

  // Function to get the type of a file from path
  std::string getType(const std::string path);

  // Function to get creation Date in tm struct
  std::tm getCreationDate();

  // Funtction to get the actual Date Time in tm struct
  std::tm nowToTm();

  // Function to get size in a human readable string
  std::string getHRSize();

 private:
  // the path to the File
  std::string _path;
  friend class Directory;

  // Type of the file
  std::string _type;

  // size of File
  uint64_t _size;

  // struct for human readable size.
  HRSize _hRSize;

  // parent folder path
  std::string _location;

  // file name
  std::string _name;
  friend class MainWindow;

  // Time file added
  std::tm _dateAdded;
};

#endif  // FILE_H_
