// Copyright 2021
// Author: Harry <daniel.h.peter@gmail.com>

#ifndef DIRECTORY_H_
#define DIRECTORY_H_

#include <QString>
#include <QTableWidgetItem>
#include <string>
#include <filesystem>
#include <vector>
#include "./File.h"
#include "./HumanReadableSize.h"

using std::vector;

// Forward declaration of File class
class File;

class Directory {
 public:
  // constructor
  explicit Directory(std::string dirPath);

  // Get Filenames from a directory (http://www.cplusplus.com/forum/windows/189681/)
  vector<std::string> get_filenames(std::filesystem::path path);

  // Get subdirs from a directory
  vector<std::string> getSubDirs(std::filesystem::path path);

  // Analyse the directory at dirPath
  void readDir(std::string path);

  // Analyse the directory at dirPath
  void analyseDir(QString dirPath);

  // Calculate the content of files in a dirPath
  uint sizeOfFiles(QString dirPath);

  // A function to convert bytes to decimal units
  void convertUnitPref(unsigned int bytes, std::string unit);

  // A function to put content to QTableWidgetItem
  vector<vector<QTableWidgetItem*>> contentToTable();

  // Put Files from an Array of pointers to a QTable WidgetItem
  vector<vector<QTableWidgetItem*>> filesToTable(vector<File*> Files);

  // A function to print the dir Content to the cout
  void prntContent();

  // A function to get the index of the smallest file of a directory
  int getSmallestFile();

  // A function to get the index of the biggest file of a directory
  int getBiggestFile();

  // A function to get a File by index
  File* getFileByIndex(int i);

  // A function to filter out files by type
  vector<File*> filterFilesByType(std::string type);

  // private:
  // Location of the directory
  std::string _dirLoc;

  // content of directory
  vector<File> _content;

  // number of Files in directory
  int _numFiles;

  // Size of all files in the directory
  uint64_t _contSize;

  // struct for human readable size.
  HRSize _hRSize;

  // unit of size
  std::string _sizeUnit;

  // Index of smallest File
  int minSizeInd;

  // Index of largest File
  int maxSizeInd;
};

#endif  // DIRECTORY_H_
