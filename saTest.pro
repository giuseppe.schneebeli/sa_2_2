TEMPLATE = app
CONFIG += console c++17
CONFIG -= app_bundle
CONFIG += thread
CONFIG -= qt

SOURCES += \
    Folder.cpp \
    folderTest.cpp

HEADERS += \
    ./Folder.h

LIBS += \
        -lgtest_main \
        -lgtest
