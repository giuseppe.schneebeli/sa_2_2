# Road Map for SA

## Goal

A tool with a user interface for managing these large amounts of files with quick overview functions (various diagrams, basic information such as minimum and maximum values of: Size and creation dates, numbers, types, ... and a log of changes)

## Planung 

### Einarbeiten 01. - 21.11.21
* Installieren der Entwicklungsumgebung und Bibliotheken
* Einrichten des Git-Repository
* Erstellen eines Zeitplan
* Beispiel Code verstehen
* Treffen mit Valère Martin am 18.11.21

### Entwurf 22.11 - 05.12.21
* GUI entwerfen
* 1. Milestone: GUI entworfen 29.12.21
* Funktionen Definieren und Auflisten
* Prioritäten Festlegen
* Treffen mit Valère Martin am 02.12.21

### Entwicklung 06.12.21 - 23.01.22
* Funktionen entwickeln
* 2. Milestone: Funktionen entwickelt 20.12.22
* Treffen mit Valère Martin am 20.12.21
* GUI entwickeln
* 3. Milestone: GUI entwickelt 20.01.22
* Treffen mit Valère Martin am 20.01.22

### Dukumentation 24.01. - 06.02.22
* Dokumentation erstellen
* Bericht schreiben
* Treffen mit Valère Martin am 03.02.22

### Abgabe 14. Januar

## Funktionen
1. Integrate json database list files and changes in directories.
2. Create tools to edit files (change names, copy, transfer, show creation date and size, ...)
3. Create a change log (e.g. when new images are added to a set)
4. Create charts (pie chart, histogram) in the user interface that summarise the content of the directories (by size, file type, creation date, etc.)
