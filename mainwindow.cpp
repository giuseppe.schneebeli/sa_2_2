// Copyright 2021
// Author: Harry <daniel.h.peter@gmail.com>

#include <QFileDialog>
#include <QTableWidget>
#include <QTableWidgetItem>
#include <QString>
#include <iostream>
#include "./mainwindow.h"
#include "./ui_mainwindow.h"
#include "./HumanReadableSize.h"
// #include "./Folder.h"
#include "./Directory.h"

// ____________________________________________________________________________
MainWindow::MainWindow(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::MainWindow) {
  ui->setupUi(this);
  _dirCount = 0;
  _sumNumFiles = 0;
  _sumSizeOfFiles = 0;
  _curDir;
}

  // ____________________________________________________________________________
MainWindow::~MainWindow() {
  delete ui;
}

void MainWindow::chooseFolder_clicked() {
  QString dirString;
  dirString = QFileDialog::getExistingDirectory(
        this,
        tr("Choose Directory"),
        QDir::homePath(),
        QFileDialog::ShowDirsOnly);
  if (!dirString.isEmpty()) {
      Directory newDir(dirString.toStdString());
    _directories.push_back(newDir);
    _dirCount += 1;
    sumUp(_directories);
    showDirs(&_directories[(_dirCount - 1)]);
  }
}

void MainWindow::showDirs(Directory* dir) {
  ui->selectedDir->insertRow((_dirCount - 1));
  QTableWidgetItem *pathItem = new QTableWidgetItem(
        QString::fromStdString(dir->_dirLoc));
  QTableWidgetItem *fileCountItem = new QTableWidgetItem(
        QString::number(dir->_numFiles));
  QTableWidgetItem *sizeItem = new QTableWidgetItem(
        (QString::number(dir->_hRSize._number) +
         QString::fromStdString(" ") +
         QString::fromStdString(dir->_hRSize._unit)));
  ui->selectedDir->setItem(_dirCount - 1, 0, pathItem);
  ui->selectedDir->setItem(_dirCount - 1, 1, fileCountItem);
  ui->selectedDir->setItem(_dirCount - 1, 2, sizeItem);
  ui->sumFilesNum->setText(QString::number(_sumNumFiles));
  ui->sumSizeNum->setText(QString::number(_hRSumSizeFiles._number));
  ui->sumSizeUnit->setText(QString::fromStdString(_hRSumSizeFiles._unit));
}

void MainWindow::prntDirs() {
  for (const auto& dir : _directories) {
      std::cout
          << dir._dirLoc
          << std::endl;
    }
}

void MainWindow::sumUp(std::vector<Directory> dirs) {
  _sumNumFiles += dirs[_dirCount - 1]._numFiles;
  _sumSizeOfFiles += dirs[_dirCount - 1]._contSize;
  _hRSumSizeFiles = toHR(_sumSizeOfFiles);
}

void MainWindow::dirEntry_clicked(QTableWidgetItem* dirRow) {
  _curDir = dirRow->row();
  std::vector<std::vector<QTableWidgetItem*>> table;
  Directory* currentDir = &_directories[_curDir];
  table = currentDir->contentToTable();
  prntDirs();
  clearTable(ui->dirContent);
  showTable(table, ui->dirContent);
  int smallestIndex = currentDir->getSmallestFile();
  int largestIndex = currentDir->getBiggestFile();
  ui->maxFilesNum->setText(
        QString::fromStdString(
          currentDir->_content[largestIndex].getHRSize()));
  ui->minFilesNum->setText(
        QString::fromStdString(
          currentDir->_content[smallestIndex].getHRSize()));
}

void MainWindow::applyFilter(QString filter) {
  Directory* currentDir = &_directories[_curDir];
  std::vector<File*> filteredFiles;
  filteredFiles = currentDir->filterFilesByType(filter.toStdString());
  std::vector<std::vector<QTableWidgetItem*>> fileItems;
  fileItems = currentDir->filesToTable(filteredFiles);
  clearTable(ui->dirContent);
  showTable(fileItems, ui->dirContent);
}

void MainWindow::showTable(std::vector<std::vector<QTableWidgetItem*>> table,
                           QTableWidget *dirContent) {
  int row = 0;
  for (auto& file : table) {
      dirContent->insertRow(row);
      int col = 0;
      for (auto& item : file) {
          dirContent->setItem(row, col, item);
          col++;
        }
      row++;
    }
}

void MainWindow::clearTable(QTableWidget *dirContent) {
  int rowNum = dirContent->rowCount();
  for (int i = 0; i <= rowNum; i++) {
      dirContent->removeRow(0);
    }
}
