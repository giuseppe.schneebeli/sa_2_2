// Copyright 2022
// Author: Harry <daniel.h.peter@gmail.com>

#include "./File.h"
#include <sys/stat.h>
#include <fstream>
#include <string>
#include <iostream>
#include <ctime>

using std::chrono::system_clock;
using std::chrono::time_point;

File::File() {}

// ___________________________________________________________________
uint64_t File::calcSize(const std::string path) {
  std::streampos begin, end;
  std::ifstream file(path, std::ios::binary);
  begin = file.tellg();
  file.seekg(0, std::ios::end);
  end = file.tellg();
  file.close();
  return (end-begin);
}

// ___________________________________________________________________
std::string File::getFileName(const std::string path) {
  std::string fileName;
  fileName = path.substr(path.find_last_of("//")+1);
  return fileName;
}

// ___________________________________________________________________
std::string File::getLocation(const std::string path) {
  std::string locationPath;
  locationPath = path.substr(0, path.find_last_of("//"));
  return locationPath;
}

// ___________________________________________________________________
std::string File::getType(const std::string path) {
  std::string fileType;
  fileType = path.substr(path.find_last_of(".")+1);
  return fileType;
}

// Function to get creation Date in tm struct
std::tm File::getCreationDate() {
  struct stat t_stat;
  stat(_path.c_str(), &t_stat);
  return *std::localtime(&t_stat.st_ctime);
}


// ____________________________________________________________________________
std::tm File::nowToTm() {
  time_point timeNow = system_clock::now();
  std::tm timeNowTm;
  std::time_t ct = std::chrono::system_clock::to_time_t(timeNow);
  timeNowTm = *std::localtime(&ct);
  return timeNowTm;
}

// Function to get size in a human readable string
std::string File::getHRSize() {
  std::string hRSize;
  hRSize = std::to_string(_hRSize._number);
  hRSize = hRSize.substr(0, hRSize.find(".")+3);
  hRSize += " ";
  hRSize += _hRSize._unit;
  return hRSize;
}
