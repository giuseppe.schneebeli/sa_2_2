// Copyright 2021
// Author: Harry <daniel.h.peter@gmail.com>


#ifndef MAINWINDOW_H_
#define MAINWINDOW_H_

#include <QMainWindow>
#include <QTableWidgetItem>
#include <vector>
// #include "./Folder.h"
#include "./HumanReadableSize.h"
#include "./Directory.h"

// Forward declaration of Folder for using it in the Mainwindow class
class Folder;

QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow {
  Q_OBJECT

 public:
  // constructor
  explicit MainWindow(QWidget *parent = nullptr);

  // destructor
  ~MainWindow();

  void showDirs(Directory* dir);

  void prntDirs();

  void showTable(std::vector<std::vector<QTableWidgetItem*>> table,
                 QTableWidget* tableWidget);

  void clearTable(QTableWidget* tableWidget);

  void sumUp(std::vector<Directory> dirs);

  public
  slots:
  void chooseFolder_clicked();

  void dirEntry_clicked(QTableWidgetItem* test);

  void applyFilter(QString filter);

 private:
  Ui::MainWindow *ui;

  // Pointer to start of array of directories
  std::vector<Directory> _directories;

  // number of directories added
  size_t _dirCount;

  // Sum of the Files of all directories
  int _sumNumFiles;

  // Sum of the Size of Files of all directories
  uint64_t _sumSizeOfFiles;

  // Sum of filesize humanreadable form
  HRSize _hRSumSizeFiles;

  // Current shown directory
  int _curDir;
};

#endif  // MAINWINDOW_H_
