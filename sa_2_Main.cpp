// Copyright 2021
// Author: Daniel Peter <daniel.peter@students.bfh.ch

#include <QApplication>

#include "./mainwindow.h"
// #include "./Folder.h"

int main(int argc, char** argv) {
    QApplication app(argc, argv);
    MainWindow mw;
    mw.show();
    return app.exec();
}
