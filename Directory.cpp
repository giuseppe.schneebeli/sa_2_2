// Copyright 2021
// Author: Harry <daniel.h.peter@gmail.com>

#include "./Directory.h"
#include <QDir>
#include <QDirIterator>
#include <iostream>
#include <fstream>
#include <filesystem>
#include <algorithm>
#include "./File.h"
#include "./HumanReadableSize.h"

using std::vector;
// ____________________________________________________________________________
Directory::Directory(std::string dirPath) {
  readDir(dirPath);
}

// ____________________________________________________________________________
void Directory::readDir(std::string path) {
  _dirLoc = path;
  int fileCounter = 0;
  uint64_t contentSize = 0;
  vector<std::string> dirsToRead;
  dirsToRead.push_back(path);
  vector<std::string> readDirs;
  while (dirsToRead.size() != 0) {
      path = dirsToRead.back();
      dirsToRead.pop_back();
      for (const auto& name : get_filenames(path)) {
          File file;
          file._path = name;
          file._size = file.calcSize(file._path);
          file._hRSize = toHR(file._size);
          file._name = file.getFileName(file._path);
          file._location = path;
          file._type = file.getType(file._path);
          file._dateAdded = file.getCreationDate();
          fileCounter++;
          contentSize += file._size;
          _content.push_back(file);
        }
      readDirs.push_back(path);
      for (const auto& dir : getSubDirs(path)) {
          dirsToRead.push_back(dir);
        }
    }
  _numFiles = fileCounter;
  _contSize = contentSize;
  _hRSize = toHR(contentSize);
}

// ____________________________________________________________________________
vector<std::string> Directory::get_filenames(std::filesystem::path path) {
  vector<std::string> fileNames;
  const std::filesystem::directory_iterator end{};
  try {
    for (std::filesystem::directory_iterator iter{path} ; iter!= end; iter++) {
        if (std::filesystem::is_regular_file(*iter)) {
            fileNames.push_back(iter->path().string());
          }
      }
  } catch(std::filesystem::filesystem_error const& ex) {
    std::cout << "error: " << ex.what() << std::endl;
  }
  return fileNames;
}

// Get subdirs from a directory
vector<std::string> Directory::getSubDirs(std::filesystem::path path) {
  vector<std::string> subDirs;
  const std::filesystem::directory_iterator end{};
  try {
    for (std::filesystem::directory_iterator iter{path} ; iter!= end; iter++) {
        if (std::filesystem::is_directory(*iter)) {
            subDirs.push_back(iter->path().string());
          }
      }
  } catch(std::filesystem::filesystem_error const& ex) {
    std::cout << "error: " << ex.what() << std::endl;
  }
  return subDirs;
}

// ____________________________________________________________________________
void Directory::analyseDir(QString dirPath) {
  QDir qDirDir;
  qDirDir.setPath(dirPath);
  _dirLoc = dirPath.toStdString();
  _numFiles = qDirDir.count();
  _contSize = sizeOfFiles(dirPath);
}

// ____________________________________________________________________________
uint Directory::sizeOfFiles(QString dirPath) {
  QDirIterator it(dirPath, QDir::Files);
  unsigned int byteCounter = 0;
  while (it.hasNext()) {
    std::streampos begin, end;
    std::ifstream file(it.next().toStdString(), std::ios::binary);
    begin = file.tellg();
    file.seekg(0, std::ios::end);
    end = file.tellg();
    file.close();
    byteCounter += (end-begin);
    }
  return byteCounter;
}

// ____________________________________________________________________________
void Directory::convertUnitPref(unsigned int bytes, std::string unit) {
  unsigned int size = bytes;
  const char *units[5] = { "B", "KB", "MB", "GB", "TB"};
  unit = units[0];
  for (int i = 1; size >= 1000; i++) {
    unit = units[i];
    size /= 1000;
  }
}

// ____________________________________________________________________________
vector<vector<QTableWidgetItem*>> Directory::contentToTable() {
  vector<vector<QTableWidgetItem*>> table;
  for (auto& file : _content) {
      vector<QTableWidgetItem*> fileCol;
      fileCol.push_back(new QTableWidgetItem(
                           QString::fromStdString(file._name)));
      fileCol.push_back(new QTableWidgetItem(
                          (QString::number(file._hRSize._number) +
                           QString::fromStdString(" ") +
                           QString::fromStdString(file._hRSize._unit))));
      fileCol.push_back(new QTableWidgetItem(
                          QString::fromStdString(
                            std::asctime(&file._dateAdded))));
      table.push_back(fileCol);
    }
  return table;
}

// ____________________________________________________________________________
vector<vector<QTableWidgetItem*>> Directory::filesToTable(vector<File*> Files) {
  vector<vector<QTableWidgetItem*>> table;
  for (auto& file : Files) {
      vector<QTableWidgetItem*> fileCol;
      fileCol.push_back(new QTableWidgetItem(
                           QString::fromStdString(file->_name)));
      fileCol.push_back(new QTableWidgetItem(
                          (QString::number(file->_hRSize._number) +
                           QString::fromStdString(" ") +
                           QString::fromStdString(file->_hRSize._unit))));
      fileCol.push_back(new QTableWidgetItem(
                          QString::fromStdString(
                            std::asctime(&file->_dateAdded))));
      table.push_back(fileCol);
    }
  return table;
}

// ____________________________________________________________________________
void Directory::prntContent() {
  for (const auto& file : _content) {
      std::cout
          << file._name
          << " "
          << file._hRSize._number
          << " "
          << file._hRSize._unit
          << " "
          << std::asctime(&file._dateAdded)
          << std::endl;
    }
}

// ____________________________________________________________________________
int Directory::getSmallestFile() {
  vector<uint64_t> fileSize;
  for (const auto& file : _content) {
     fileSize.push_back(file._size);
    }
  int minIndex = std::min_element(
        fileSize.begin(), fileSize.end()) - fileSize.begin();
  return minIndex;
}

// ____________________________________________________________________________
int Directory::getBiggestFile() {
  vector<uint64_t> fileSize;
  for (const auto& file : _content) {
     fileSize.push_back(file._size);
    }
  int maxIndex = std::max_element(
        fileSize.begin(), fileSize.end()) - fileSize.begin();
  return maxIndex;
}

// ____________________________________________________________________________
File* Directory::getFileByIndex(int i) {
  File* file;
  file = &_content[i];
  return file;
}

// ____________________________________________________________________________
vector<File*> Directory::filterFilesByType(std::string type) {
  vector<std::string> fileEndings;
  if (type == "Text") {
      fileEndings = {"txt"};
    } else if (type == "Images") {
      fileEndings = {"jpg", "jpeg", "jpe", "jfif", "gif",
                     "png", "bmp", "tiff", "tif"};
    } else if (type == "Video") {
      fileEndings = {"mp4", "mov", "qt", "divx", "webm",
                     "mkv", "mka", "asf", "wmv", "wma",
                     "avi", "mpg", "mpeg", "3gp", "ogg", "ogv"};
    } else {
      fileEndings = {};
    }
  std::sort(fileEndings.begin(), fileEndings.end());
  vector<File*> filteredFiles;
  for (auto& file : _content) {
      if (std::binary_search(fileEndings.begin(),
                             fileEndings.end(),
                             file._type)) {
          filteredFiles.push_back(&file);
        }
    }
  return filteredFiles;
}
