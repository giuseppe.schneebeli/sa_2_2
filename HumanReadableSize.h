// Copyright 2022
// Author: Harry <daniel.h.peter@gmail.com>

#ifndef HUMANREADABLESIZE_H_
#define HUMANREADABLESIZE_H_

#include <string>

struct HRSize {
  double _number;
  std::string _unit;
};

HRSize toHR(uint64_t bytes);

#endif  // HUMANREADABLESIZE_H_
